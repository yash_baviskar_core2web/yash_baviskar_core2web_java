class BitwiseOperator{

	public static void main(String[] args){

	int x = 35;				// 0 0 1 0  0 0 1 1 

	int y = 21;				// 0 0 0 1  0 1 0 1

	System.out.println(x & y); 	// 1 i.e.  0 0 0 0  0 0 0 1 

	System.out.println(x | y); 	// 55 i.e. 0 0 1 1  0 1 1 1

}

}
