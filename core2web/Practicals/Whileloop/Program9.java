class Whiledemo{
	public static void main(String[] args){
		int num = 214367689;
		
		int cnt1 = 0;
		int cnt2 = 0;
			
		while(num>0){
			int digit = num%10;
			if(digit%2==1){
				cnt1++;
			}
			else{
				cnt2++;
			}
			num/=10;
		}
		System.out.println("Odd count is " + cnt1);
		System.out.println("even count is " + cnt2);	
	}
	 
}
