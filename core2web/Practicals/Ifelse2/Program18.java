class Student{
	public static void main(String[]args){
		 
		float percentage = 85.00f;
		if(percentage >= 75.00){
			System.out.println("Passed : first class with distinction");
		}
		else if (percentage >= 60.00 && percentage < 70.00){
			System.out.println("Passed : first class");
		}
		else if (percentage >= 50.00 && percentage < 60.00){
			System.out.println("Passed : second class");
		}
		else if (percentage >= 35.00 && percentage < 50.00){
			 System.out.println("Passed");
		}
		else {
		        System.out.println("Failed with distinction");
		}
	}
}	
