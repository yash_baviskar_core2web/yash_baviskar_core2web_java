import java.io.*;
class Pattern{
        public static void main(String[] args)throws IOException{
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter Rows : ");
                int row = Integer.parseInt(br.readLine());
                for(int i =1;i<=row;i++){
			 for(int j =1;j<=i;j++){
				 int ch = row+65;

				  System.out.print((char)ch + " ");
				  ch++;
			 }
			  System.out.println();
		}
	}
}
