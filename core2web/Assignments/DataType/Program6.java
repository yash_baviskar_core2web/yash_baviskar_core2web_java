class Person{

	public static void main(String[] args){

		byte date = 24;

		byte month = 9;

		short year = 2001;

		int seconds_ina_day = 86400;

		System.out.println("date :" + date);
		System.out.println("month :" + month);
		System.out.println("year :" + year);

		System.out.println("Seconds in a day are " + seconds_ina_day);

		seconds_ina_day *= 30;

		System.out.println("Seconds in a month are " + seconds_ina_day);

		seconds_ina_day *= 12;

		System.out.println("Seconds in a year are " + seconds_ina_day);

	}

}
