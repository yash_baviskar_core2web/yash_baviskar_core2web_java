
class Temperature{

	public static void main(String[] args){
        
        double acTemperature = 18; 
        int standardRoomTemperature = 24; 

        System.out.println("Temperature of the Air Conditioner: " + acTemperature + " degrees");
        System.out.println("Standard Room Temperature: " + standardRoomTemperature + " degrees");

	}
}
