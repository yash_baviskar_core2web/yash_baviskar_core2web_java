
class Gravity{

	public static void main(String[] args){

		double gravity = 9.81;

		char ch = 'g';

		System.out.println("Gravity : " + gravity + "\n" + "Acceleration due to gravity is represented by " + ch);

	}

}
